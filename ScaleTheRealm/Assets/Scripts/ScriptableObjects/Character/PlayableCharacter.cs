﻿using Assets.Scripts.non_behaviour;
using UnityEngine;

namespace Assets.Scripts.ScriptableObjects.Character
{
    [CreateAssetMenu(menuName = "Character/playable")]
    class PlayableCharacter : CombatCharacter
    {
        [SerializeField] Equipment equipment;
    }
}
