﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Core
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField] KeyBindings keyBindings;

        public static InputManager instance;

        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
            DontDestroyOnLoad(this);
        }

        public bool getKeyDown(string keyName)
        {
            if(Input.GetKeyDown(keyBindings.getKeyCode(keyName)))
            {
                return true;
            }
            return false;
        }

        public bool getKey(string keyName)
        {
            if (Input.GetKey(keyBindings.getKeyCode(keyName)))
            {
                return true;
            }
            return false;
        }

        public bool getKeyUp(string keyName)
        {
            if (Input.GetKey(keyBindings.getKeyCode(keyName)))
            {
                return true;
            }
            return false;
        }
    }

    
}