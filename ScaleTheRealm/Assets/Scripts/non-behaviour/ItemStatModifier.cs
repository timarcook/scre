﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.non_behaviour
{
    [Serializable]
    public class ItemStatModifier
    {
        public StatEnum statEnum;
        public StatModifier statModifier;
    }
}
