﻿using UnityEngine;
[CreateAssetMenu]
public class ScriptableObjectReset : ScriptableObject
{
    public FloatReference[] floatReferences;
    private float[] floats;

    public void LoadAll()
    {

        floats = new float[floatReferences.Length];
        for (int i = 0; i < floatReferences.Length; i++)
        {
            floats[i] = floatReferences[i].Value;
        }
    }

    public void RestoreAll()
    {
        for (int i = 0; i < floatReferences.Length; i++)
        {
            floatReferences[i].Value = floats[i];
        }
    }
}
