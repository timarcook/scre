﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.ScriptableObjects.Character;

namespace Assets.Scripts.Behaviour.UI
{
    public class CharcterScreenBehaviour : MonoBehaviour
    {
        [SerializeField] Text healthText;
        [SerializeField] Text strText;
        [SerializeField] Text dexText;
        [SerializeField] Text intText;
        [SerializeField] Text speedText;

        [SerializeField] PlayableCharacter data;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void UpdateStats()
        {
            
        }
    }
}