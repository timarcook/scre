﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectResetBehaviour : MonoBehaviour
{
    public ScriptableObjectReset SOR;

    private void OnEnable()
    {
        SOR.LoadAll();
    }

    private void OnDisable()
    {
        SOR.RestoreAll();
    }
}
