﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.non_behaviour;

namespace Assets.Scripts.ScriptableObjects.Character
{
    public abstract class CombatCharacter : Character
    {
        public float damage;

        public float armor;

        public float strength;

        public float dextirity;

        public float intelligence;

        public float vitality;

        public float actionPoints;
    }
}