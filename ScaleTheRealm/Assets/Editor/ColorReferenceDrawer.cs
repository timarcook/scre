﻿
using UnityEngine;
using UnityEditor;
[CustomPropertyDrawer(typeof(ColorReference))]
public class ColorReferenceDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //base.OnGUI(position, property, label);
        EditorGUI.BeginProperty(position,label,property);
        bool useConstant = property.FindPropertyRelative("UseConstant").boolValue;

        //Label
        position = EditorGUI.PrefixLabel(position,GUIUtility.GetControlID(FocusType.Passive), label);

        var rect = new Rect(position.position, Vector2.one * 20);

        if(EditorGUI.DropdownButton(rect,new GUIContent(EditorGUIUtility.IconContent("align_vertically_center")),FocusType.Keyboard, new GUIStyle() {fixedWidth = 50f, border = new RectOffset(1,1,1,1)}))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Constant"), useConstant, () => SetProperty(property, true));
            menu.AddItem(new GUIContent("Variable"), !useConstant, () => SetProperty(property, false));

            menu.ShowAsContext();
        }

        position.position += Vector2.right * 15;
        Color value = property.FindPropertyRelative("ConstantValue").colorValue;

        if(useConstant)
        {
            Color newValue = EditorGUI.ColorField(position, value);
            value = newValue;
            property.FindPropertyRelative("ConstantValue").colorValue = value;
        }
        else
        {
            EditorGUI.ObjectField(position, property.FindPropertyRelative("Variable"),GUIContent.none);
        }
        EditorGUI.EndProperty();
    }


    private void SetProperty(SerializedProperty property, bool value)
    {
        var propRelative = property.FindPropertyRelative("UseConstant");
        propRelative.boolValue = value;
        property.serializedObject.ApplyModifiedProperties();
    }
}
