﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.ScriptableObjects.Character
{
    public abstract class Character : ScriptableObject
    {
        public string name;

        public Sprite sprite;

        public virtual void Init() { }
    }
}