﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "KeyBinds")]
public class KeyBindings : ScriptableObject
{
    public float senseitivty = 1.0f;

    public KeyCode attack = KeyCode.Mouse0;
    public KeyCode altAttack = KeyCode.Mouse1;
    public KeyCode left = KeyCode.A;
    public KeyCode right = KeyCode.D;
    public KeyCode jump = KeyCode.Space;

    public KeyCode abillity1 = KeyCode.Alpha1;


    public KeyCode pause = KeyCode.Escape;


    public KeyCode getKeyCode(string keyName)
    {
        switch(keyName)
        {
            case "attack":
                return attack;
            case "altAttack":
                return altAttack;
            case "jump":
                return jump;
            case "left":
                return left;
            case "right":
                return right;
            default:
                return KeyCode.None;
        }
    }
}
