﻿using UnityEngine;
[System.Serializable]
public class ColorReference
{
    public bool UseConstant = true;
    public Color ConstantValue;
    public ColorVariable Variable;


    public Color Value
    {
        get { return UseConstant ? ConstantValue : Variable.Value; }
        set
        {
            if (UseConstant)
                ConstantValue = value;
            else
                Variable.Value = value;
        }
    }
}
