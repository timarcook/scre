﻿using Assets.Scripts.ScriptableObjects.Item;
using Assets.Scripts.non_behaviour;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.ScriptableObjects.Item
{

    public enum EquipmentSlot { weapon,head,chest,legs}
    [CreateAssetMenu(menuName = "Equipment")]
    public class EquippableItem : Item
    {
        public EquipmentSlot slot;
        public List<ItemStatModifier> statModifiers;
    }
}
