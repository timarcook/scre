﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.non_behaviour
{

    public enum StatEnum  {damage,armor, strength, dextirity, intelligence, vitality, actionPoints};
    public enum ShortStatEnum {Dmg,Arm,Str,Dex,Int,Vit,AP};

    [System.Serializable]
    public class Statsheet
    {
        public Stat damage;

        public Stat armor;

        public Stat strength;

        public Stat dextirity;

        public Stat intelligence;

        public Stat vitality;

        public Stat actionPoints;

        protected Dictionary<StatEnum, Stat> statDictionary;
        protected List<Stat> stats;

        public Statsheet()
        {
            statDictionary = new Dictionary<StatEnum, Stat>();
            stats = new List<Stat>();
            for(int i = 0; i < System.Enum.GetNames(typeof(StatEnum)).Length; i++)
            {

            }
            //statDictionary.Add(StatEnum.damage, damage);
            //statDictionary.Add(StatEnum.armor, armor);
            //statDictionary.Add(StatEnum.strength, strength);
            //statDictionary.Add(StatEnum.dextirity, dextirity);
            //statDictionary.Add(StatEnum.intelligence, intelligence);
            //statDictionary.Add(StatEnum.vitality, vitality);
            //statDictionary.Add(StatEnum.actionPoints, actionPoints);
        }

        public void addModifier(StatModifier mod, StatEnum statEnum)
        {
            Stat stat;
            statDictionary.TryGetValue(statEnum, out stat);
            stat.AddModifier(mod);
        }
    }



}
