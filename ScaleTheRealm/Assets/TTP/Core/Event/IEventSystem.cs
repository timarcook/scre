﻿using UnityEngine.Events;
using UnityEngine;
using System.Collections.Generic;

public interface IEventSystem
{
    void StartListening(string eventName, UnityAction listener);

    void StopListening(string eventName, UnityAction listener);

    void TriggerEvent(string eventName);

}

public interface IEventSystem<T>
{
    void StartListening(string eventName, UnityAction<T> listener);

    void StopListening(string eventName, UnityAction<T> listener);

    void TriggerEvent(string eventName, T parameter);

}

public interface IEventSystem<T0,T1>
{
    void StartListening(string eventName, UnityAction<T0, T1> listener);

    void StopListening(string eventName, UnityAction<T0, T1> listener);

    void TriggerEvent(string eventName, T0 parameter0, T1 parameter1);
}

public interface IEventSystem<T0, T1, T2>
{
    void StartListening(string eventName, UnityAction<T0, T1, T2> listener);

    void StopListening(string eventName, UnityAction<T0, T1, T2> listener);

    void TriggerEvent(string eventName, T0 parameter0, T1 parameter1, T2 parameter2);

}

public interface IEventSystem<T0, T1, T2, T3>
{
    void StartListening(string eventName, UnityAction<T0, T1, T2, T3> listener);

    void StopListening(string eventName, UnityAction<T0, T1, T2, T3> listener);

    void TriggerEvent(string eventName, T0 parameter0, T1 parameter1, T2 parameter2, T3 parameter3);

}
