﻿using Assets.Scripts.non_behaviour.CombatState;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Behaviour
{
    public class BattleStateMachine : MonoBehaviour
    {
        public BattleState state;

        public void switchState(BattleState newState)
        {
            if(state != null)
            {
                state.Exit();
            }
            state = newState;
            state.Enter();
        }
    }
}
