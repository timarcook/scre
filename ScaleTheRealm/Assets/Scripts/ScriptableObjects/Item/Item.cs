﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.ScriptableObjects.Item
{
    public abstract class Item : ScriptableObject
    {
        public string name = "Item";
        public string description = "Nothing unsual";

        public int buyCost = 100;
        public int sellCost = 50;

        public Sprite sprite;
    }
}