﻿using Assets.Scripts.Behaviour;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.non_behaviour.CombatState
{
    public abstract class BattleState
    {
        protected BattleController controller;
        public BattleState (BattleController controller)
        {
            this.controller = controller;
        }
        public virtual IEnumerator Enter() { yield return null;  }

        public virtual IEnumerator Exit() { yield return null; }

        public virtual void Update() { }
    }
}
