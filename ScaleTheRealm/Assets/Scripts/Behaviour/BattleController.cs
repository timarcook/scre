﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Assets.Scripts.Behaviour
{
    public class BattleController : BattleStateMachine
    {
        public static BattleController instance;
        public CharacterBehaviour player;
        public CharacterBehaviour enemy;
        //[HideInInspector] public List<CharacterBehaviour> PlayerParty = new List<CharacterBehaviour>();
        //[HideInInspector] public List<CharacterBehaviour> EnemyParty = new List<CharacterBehaviour>();
        // Use this for initialization
        void Start()
        {
            if(instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }
    }
}