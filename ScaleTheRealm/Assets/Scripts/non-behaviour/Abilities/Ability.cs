﻿using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.non_behaviour.Abilities
{
    [System.Serializable]
    public abstract class Ability
    {
        public string abilityName;
        public string description;
        public int APCost;
        public abstract IEnumerator Cast();
    }
}
