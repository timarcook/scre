﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.non_behaviour
{
    [Serializable]
    public class Stat
    {
        public float BaseValue;
        protected float lastBaseValue = float.MinValue;
        protected float _value;

        protected readonly List<StatModifier> statModifiers;
        public readonly ReadOnlyCollection<StatModifier> StatModifiers;

        protected bool isDirty = true;

        public float Value
        {
            get
            {
                if (isDirty || lastBaseValue != BaseValue)
                {
                    lastBaseValue = BaseValue;
                    _value = CalculateFinalValue();
                    isDirty = false;
                }
                return _value;
            }
        }

        public Stat(float baseValue)
        {
            this.BaseValue = baseValue;
            statModifiers = new List<StatModifier>();
            StatModifiers = statModifiers.AsReadOnly();
        }

        public Stat()
        {
            isDirty = true;
            statModifiers = new List<StatModifier>();
        }

        protected virtual float CalculateFinalValue()
        {
            float finalValue = BaseValue;
            float sumPrecentAdd = 0;
            for (int i = 0; i < statModifiers.Count; i++)
            {
                StatModifier mod = statModifiers[i];
                if(mod.Type == StatModType.Flat)
                {
                    finalValue += mod.Value;
                }
                else if(mod.Type == StatModType.PercentAdd)
                {
                    sumPrecentAdd += mod.Value;
                    if(i + 1 >= statModifiers.Count || statModifiers[i + 1].Type != StatModType.PercentAdd)
                    {
                        finalValue *= 1 + sumPrecentAdd;
                        sumPrecentAdd = 0;
                    }
                }
                else if(mod.Type == StatModType.PercentMult)
                {
                    finalValue *= 1 + mod.Value;
                }
              
            }
            return (float) Math.Round(finalValue,4);
        }

        public bool RemoveAllModifiersFromSource(object source)
        {
            bool didRemove = false;

            for (int i = statModifiers.Count - 1; i >= 0; i--)
            {
                if (statModifiers[i].Source == source)
                {
                    isDirty = true;
                    didRemove = true;
                    statModifiers.RemoveAt(i);
                }
            }
            return didRemove;
        }

        public virtual void AddModifier(StatModifier mod)
        {
            isDirty = true;
            statModifiers.Add(mod);
            statModifiers.Sort(CompareModifierOrder);
        }

        public bool RemoveModifier(StatModifier mod)
        {
            if (statModifiers.Remove(mod))
            {
                isDirty = true;
                return true;
            }
            return false;
        }

        protected int CompareModifierOrder(StatModifier a, StatModifier b)
        {
            if (a.Order < b.Order)
                return -1;
            else if (a.Order > b.Order)
                return 1;
            return 0;
        }


    }
}
