﻿using UnityEngine;
using System.Collections;

public interface IEventUser
{
    void RegisterEvents(IEventSystem eventSystem);
}

public interface IEventUser<T>
{
    void RegisterEvents(IEventSystem<T> eventSystem);
}

public interface IEventUser<T0,T1>
{
    void RegisterEvents(IEventSystem<T0, T1> eventSystem);
}

public interface IEventUser<T0, T1, T2>
{
    void RegisterEvents(IEventSystem<T0, T1, T2> eventSystem);
}

public interface IEventUser<T0, T1, T2, T3>
{
    void RegisterEvents(IEventSystem<T0, T1, T2, T3> eventSystem);
}
