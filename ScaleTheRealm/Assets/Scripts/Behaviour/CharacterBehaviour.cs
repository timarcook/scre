﻿using UnityEngine;
using System;
using Assets.Scripts.ScriptableObjects.Character;
using Assets.Scripts.non_behaviour;

namespace Assets.Scripts.Behaviour
{
    public class CharacterBehaviour : MonoBehaviour
    {
        [SerializeField] CombatCharacter combatCharacter;

        [HideInInspector] public Stat damage;

        [HideInInspector] public Stat armor;

        [HideInInspector] public Stat strength;

        [HideInInspector] public Stat dextirity;

        [HideInInspector] public Stat intelligence;

        [HideInInspector] public Stat vitality;

        [HideInInspector] public Stat actionPoints;

        [HideInInspector] public float Health;


        // Use this for initialization
        void Start()
        {
            damage = new Stat(combatCharacter.damage);
            armor = new Stat(combatCharacter.armor);
            strength = new Stat(combatCharacter.strength);
            dextirity = new Stat(combatCharacter.dextirity);
            intelligence = new Stat(combatCharacter.intelligence);
            vitality = new Stat(combatCharacter.vitality);
            actionPoints = new Stat(combatCharacter.actionPoints);
            Health = vitality.Value * 10;
        }

        public void TakeDamage(float damage)
        {
            Health -= damage;
            Debug.Log(combatCharacter.name + " took " + damage + " damage remaning health: " + Health);
        }   
    }
}