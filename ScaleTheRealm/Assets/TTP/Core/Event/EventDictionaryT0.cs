﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventDictionary<T> : IEventSystem<T>
{
    Dictionary<string, UnityEvent<T>> eventDictionary;


    public EventDictionary()
    {
        eventDictionary = new Dictionary<string, UnityEvent<T>>();
    }

    public void TriggerEvent(string eventName, T parameter)
    {
        UnityEvent<T> thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(parameter);
        }
    }

    public void StartListening(string eventName, UnityAction<T> listener)
    {
        UnityEvent<T> thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent<T>();
            thisEvent.AddListener(listener);
            eventDictionary.Add(eventName, thisEvent);
        }
    }

    public void StopListening(string eventName, UnityAction<T> listener)
    {
        UnityEvent<T> thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

}

