﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventDictionary<T0,T1> : IEventSystem<T0, T1>
{
    Dictionary<string, UnityEvent<T0,T1>> eventDictionary;


    public EventDictionary()
    {
        eventDictionary = new Dictionary<string, UnityEvent<T0,T1>>();
    }

    public void TriggerEvent(string eventName, T0 parameter0, T1 parameter1)
    {
        UnityEvent<T0,T1> thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(parameter0,parameter1);
        }
    }

    public void StartListening(string eventName, UnityAction<T0, T1> listener)
    {
        UnityEvent<T0, T1> thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent<T0, T1>();
            thisEvent.AddListener(listener);
            eventDictionary.Add(eventName, thisEvent);
        }
    }

    public void StopListening(string eventName, UnityAction<T0, T1> listener)
    {
        UnityEvent<T0, T1> thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }
}

