﻿using Assets.Scripts.Behaviour;
using System.Collections;

namespace Assets.Scripts.non_behaviour.CombatState
{
    class PlayerTurnState : BattleState
    {
        public PlayerTurnState(BattleController controller) : base(controller)
        {
        }

        public override IEnumerator Enter()
        {
            return base.Enter();
        }

        public override IEnumerator Exit()
        {
            return base.Exit();
        }
    }
}
