﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.non_behaviour
{
    public enum StatModType
    {
        Flat = 100,
        PercentAdd = 200,
        PercentMult = 300,

    }
    [Serializable]
    public class StatModifier
    {
        public readonly float Value;
        public readonly StatModType Type;
        public readonly int Order;
        public readonly object Source;


        public StatModifier(float value, StatModType type, int order, object source)
        {
            this.Value = value;
            this.Type = type;
            this.Order = order;
            this.Source = source;
        }

        public StatModifier(float value, StatModType type) : this(value, type, (int)type, null) { }
        public StatModifier(float value) : this(value, StatModType.Flat, (int)StatModType.Flat, null) { }
        public StatModifier(float value, StatModType type, object source) : this(value, type, (int)type, source) { }
        public StatModifier(float value, StatModType type, int order) : this(value, type, order, null) { }
    }
}
