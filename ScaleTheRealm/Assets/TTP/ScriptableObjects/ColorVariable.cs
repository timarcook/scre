﻿using UnityEngine;

[CreateAssetMenu(fileName = "ColorVariable", menuName = "Variable/Color", order = 2)]
public class ColorVariable : ScriptableObject
{
    public Color Value;
}
