﻿using UnityEngine;
[CreateAssetMenu(fileName = "FloatVariable", menuName = "Variable/Float", order = 1)]
public class FloatVariable : ScriptableObject
{
    public float Value;
}
